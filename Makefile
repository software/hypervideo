all: hypervideo README.md CONTRIBUTING.md README.txt hypervideo.1 hypervideo.bash-completion hypervideo.zsh hypervideo.fish supportedsites

clean:
	rm -rf hypervideo.1.temp.md hypervideo.1 hypervideo.bash-completion README.txt MANIFEST build/ dist/ .coverage cover/ hypervideo.tar.gz hypervideo.zsh hypervideo.fish hypervideo/extractor/lazy_extractors.py *.dump *.part* *.ytdl *.info.json *.mp4 *.m4a *.flv *.mp3 *.avi *.mkv *.webm *.3gp *.wav *.ape *.swf *.jpg *.png CONTRIBUTING.md.tmp hypervideo.exe
	find . -name "*.pyc" -delete
	find . -name "*.class" -delete

PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/man
SHAREDIR ?= $(PREFIX)/share
PYTHON ?= /usr/bin/env python

# set SYSCONFDIR to /etc if PREFIX=/usr or PREFIX=/usr/local
SYSCONFDIR = $(shell if [ $(PREFIX) = /usr -o $(PREFIX) = /usr/local ]; then echo /etc; else echo $(PREFIX)/etc; fi)

# set markdown input format to "markdown-smart" for pandoc version 2 and to "markdown" for pandoc prior to version 2
MARKDOWN = $(shell if [ `pandoc -v | head -n1 | cut -d" " -f2 | head -c1` = "2" ]; then echo markdown-smart; else echo markdown; fi)

install: hypervideo hypervideo.1 hypervideo.bash-completion hypervideo.zsh hypervideo.fish
	install -d $(DESTDIR)$(BINDIR)
	install -m 755 hypervideo $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(MANDIR)/man1
	install -m 644 hypervideo.1 $(DESTDIR)$(MANDIR)/man1
	install -d $(DESTDIR)$(SYSCONFDIR)/bash_completion.d
	install -m 644 hypervideo.bash-completion $(DESTDIR)$(SYSCONFDIR)/bash_completion.d/hypervideo
	install -d $(DESTDIR)$(SHAREDIR)/zsh/site-functions
	install -m 644 hypervideo.zsh $(DESTDIR)$(SHAREDIR)/zsh/site-functions/_hypervideo
	install -d $(DESTDIR)$(SYSCONFDIR)/fish/completions
	install -m 644 hypervideo.fish $(DESTDIR)$(SYSCONFDIR)/fish/completions/hypervideo.fish

codetest:
	flake8 .

test:
	nosetests --verbose test
	$(MAKE) codetest

ot: offlinetest

# Keep this list in sync with devscripts/run_tests.sh
offlinetest: codetest
	$(PYTHON) -m nose --verbose test \
		--exclude test_age_restriction.py \
		--exclude test_download.py \
		--exclude test_iqiyi_sdk_interpreter.py \
		--exclude test_socks.py \
		--exclude test_subtitles.py \
		--exclude test_write_annotations.py \
		--exclude test_youtube_lists.py \
		--exclude test_youtube_signature.py

tar: hypervideo.tar.gz

.PHONY: all clean install test tar bash-completion pypi-files zsh-completion fish-completion ot offlinetest codetest supportedsites

pypi-files: hypervideo.bash-completion README.txt hypervideo.1 hypervideo.fish

hypervideo: hypervideo/*.py hypervideo/*/*.py
	mkdir -p zip
	for d in hypervideo hypervideo/downloader hypervideo/extractor hypervideo/postprocessor ; do \
	  mkdir -p zip/$$d ;\
	  cp -pPR $$d/*.py zip/$$d/ ;\
	done
	touch -t 200001010101 zip/hypervideo/*.py zip/hypervideo/*/*.py
	mv zip/hypervideo/__main__.py zip/
	cd zip ; zip -q ../hypervideo hypervideo/*.py hypervideo/*/*.py __main__.py
	rm -rf zip
	echo '#!$(PYTHON)' > hypervideo
	cat hypervideo.zip >> hypervideo
	rm hypervideo.zip
	chmod a+x hypervideo

README.md: hypervideo/*.py hypervideo/*/*.py
	COLUMNS=80 $(PYTHON) hypervideo/__main__.py --help | $(PYTHON) devscripts/make_readme.py

CONTRIBUTING.md: README.md
	$(PYTHON) devscripts/make_contributing.py README.md CONTRIBUTING.md

supportedsites:
	$(PYTHON) devscripts/make_supportedsites.py docs/supportedsites.md

README.txt: README.md
	pandoc -f $(MARKDOWN) -t plain README.md -o README.txt

hypervideo.1: README.md
	$(PYTHON) devscripts/prepare_manpage.py hypervideo.1.temp.md
	pandoc -s -f $(MARKDOWN) -t man hypervideo.1.temp.md -o hypervideo.1
	rm -f hypervideo.1.temp.md

hypervideo.bash-completion: hypervideo/*.py hypervideo/*/*.py devscripts/bash-completion.in
	$(PYTHON) devscripts/bash-completion.py

bash-completion: hypervideo.bash-completion

hypervideo.zsh: hypervideo/*.py hypervideo/*/*.py devscripts/zsh-completion.in
	$(PYTHON) devscripts/zsh-completion.py

zsh-completion: hypervideo.zsh

hypervideo.fish: hypervideo/*.py hypervideo/*/*.py devscripts/fish-completion.in
	$(PYTHON) devscripts/fish-completion.py

fish-completion: hypervideo.fish

lazy-extractors: hypervideo/extractor/lazy_extractors.py

_EXTRACTOR_FILES = $(shell find hypervideo/extractor -iname '*.py' -and -not -iname 'lazy_extractors.py')
hypervideo/extractor/lazy_extractors.py: devscripts/make_lazy_extractors.py devscripts/lazy_load_template.py $(_EXTRACTOR_FILES)
	$(PYTHON) devscripts/make_lazy_extractors.py $@

hypervideo.tar.gz: hypervideo README.md README.txt hypervideo.1 hypervideo.bash-completion hypervideo.zsh hypervideo.fish ChangeLog AUTHORS
	@tar -czf hypervideo.tar.gz --transform "s|^|hypervideo/|" --owner 0 --group 0 \
		--exclude '*.DS_Store' \
		--exclude '*.kate-swp' \
		--exclude '*.pyc' \
		--exclude '*.pyo' \
		--exclude '*~' \
		--exclude '__pycache__' \
		--exclude '.git' \
		--exclude 'docs/_build' \
		-- \
		bin devscripts test hypervideo docs \
		ChangeLog AUTHORS LICENSE README.md README.txt \
		Makefile MANIFEST.in hypervideo.1 hypervideo.bash-completion \
		hypervideo.zsh hypervideo.fish setup.py setup.cfg
